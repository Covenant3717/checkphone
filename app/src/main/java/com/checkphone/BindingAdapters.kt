package com.checkphone

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter


// Kotlin-файл для кастомных параметров для биндинга в xml-разметку

@BindingAdapter("app:imageSrc")
fun setImage(view: ImageView, resource: Int) {
    view.setImageResource(resource)
}
