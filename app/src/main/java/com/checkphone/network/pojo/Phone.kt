package com.checkphone.network.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Phone() {

    @SerializedName("success")
    @Expose(serialize = false)
    var success: Boolean = true

    @SerializedName("mno")
    @Expose(serialize = false)
    var mno: String = ""

    @SerializedName("mnoText")
    @Expose(serialize = false)
    var mnoText: String = ""


    @SerializedName("errorCode")
    @Expose(serialize = false)
    var errorCode: Int = 0

    @SerializedName("errorText")
    @Expose(serialize = false)
    var errorText: String = ""

}

