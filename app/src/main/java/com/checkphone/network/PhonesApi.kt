package com.checkphone.network

import com.checkphone.network.pojo.Phone
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit
// https://pay.payfon24.ru/mno?phone=9082049535

private const val BASE_URL = "https://pay.payfon24.ru/"

interface PhonesApi {

    object Retrofit2 {
        var interceptor: HttpLoggingInterceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

        private fun makeOkHttpClient(): OkHttpClient {
            return OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .build()
        }

        fun makeRetrofitService(): PhonesApi {
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(makeOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()
                .create(PhonesApi::class.java)
        }
    }


    @Headers("Content-Type: application/json")
    @GET("mno")
    fun requestCheckPhone(@Query("phone") query: String): Deferred<Response<Phone>>
}