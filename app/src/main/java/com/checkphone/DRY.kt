package com.checkphone

import android.widget.EditText
import ru.tinkoff.decoro.MaskImpl
import ru.tinkoff.decoro.parser.PhoneNumberUnderscoreSlotsParser
import ru.tinkoff.decoro.parser.UnderscoreDigitSlotsParser
import ru.tinkoff.decoro.watchers.MaskFormatWatcher

// маска для номера телефона
fun setMaskPhone(editText: EditText) {
    val slots = UnderscoreDigitSlotsParser().parseSlots("+7 (___) ___-__-__")
    val mask = MaskImpl.createTerminated(slots)
    val watcher = MaskFormatWatcher(mask)
    watcher.installOn(editText)
}

// очищает маску для телефона от мишуры типа скобок и т.д, оставляя лишь данные
fun cleanMaskPhone(string: String): String {
    val slots = PhoneNumberUnderscoreSlotsParser().parseSlots("+7 (___) ___-__-__")
    val inputMask = MaskImpl.createTerminated(slots)
    inputMask.insertFront(string)
    return inputMask.toUnformattedString()

}