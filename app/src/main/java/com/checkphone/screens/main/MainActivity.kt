package com.checkphone.screens.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.jaeger.library.StatusBarUtil
import android.view.WindowManager
import android.widget.EditText
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.checkphone.R
import com.checkphone.databinding.ActivityMainBinding
import com.checkphone.setMaskPhone
import kotlinx.android.synthetic.main.activity_main.*
import ru.tinkoff.decoro.MaskImpl
import ru.tinkoff.decoro.parser.PhoneNumberUnderscoreSlotsParser
import ru.tinkoff.decoro.parser.UnderscoreDigitSlotsParser
import ru.tinkoff.decoro.watchers.MaskFormatWatcher


class MainActivity : AppCompatActivity() {

    private val binding by lazy { DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main) }
    private val mainViewModel by lazy { ViewModelProviders.of(this).get(MainViewModel::class.java) }

    //==================================================================================================================

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initMain()
    }

    //==================================================================================================================

    private fun initMain() {
        // set transparent statusBar and move view up, under statusBar
        StatusBarUtil.setTransparent(this)
        val w = window
        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        binding.mainViewModel = mainViewModel
        binding.lifecycleOwner = this

        setMaskPhone(main_ed_phone)

        main_btn_check.setOnClickListener {
            mainViewModel.checkPhone(this)
        }
    }



}
