package com.checkphone.screens.main

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.checkphone.R
import com.checkphone.cleanMaskPhone
import com.checkphone.network.pojo.Phone

class MainViewModel() : ViewModel() {
    companion object {
        val viewModelLiveData: MutableLiveData<Phone> by lazy { MutableLiveData<Phone>() }
    }
    private val model by lazy { MainModel() }

    val progress by lazy { ObservableInt(View.GONE) }
    val logo by lazy { ObservableInt(R.drawable.placeholder) }
    val tvOperator by lazy { ObservableField<String>("-") }
    val tvPhone by lazy { ObservableField<String>("-") }
    val edPhone by lazy { ObservableField<String>("") }
    val btnCheckEnabled by lazy { ObservableBoolean(true) }

    //==================================================================================================================

    init {

    }

    fun checkPhone(lifecycle: LifecycleOwner) {
        Log.d("ml", "//=============================================")

        // clean phone mask
        val phone = edPhone.get()?.let { cleanMaskPhone(it) }

        // remove +7 from number
        val cleanedPhone = phone?.replaceRange(0, 2, "")

        if (cleanedPhone!!.isNotEmpty()) {
            // clean previous data
            progress.set(View.VISIBLE)
            btnCheckEnabled.set(false)
            tvOperator.set("-")
            tvPhone.set("-")

            model.requestCheckPhone(cleanedPhone)

        } else {
            Toast.makeText(lifecycle as Context, "Введите номер", Toast.LENGTH_SHORT).show()
        }
    }

    private fun responseListener(lifecycle: LifecycleOwner) {
        model.phoneData.observe(lifecycle, Observer {
            Log.d("ml", "Observer{ }")

            progress.set(View.GONE)
            btnCheckEnabled.set(true)

            if (it.success) {
                Log.d("ml", "Observer{ } success")

                tvOperator.set(it.mnoText)
                tvPhone.set(edPhone.get())
                edPhone.set("")

                setLogo(it.mnoText)
            } else {
                Toast.makeText(lifecycle as Context, it.errorText, Toast.LENGTH_SHORT).show()
            }

        })
    }

    private fun setLogo(operator: String) {
        when (operator.toLowerCase()) {
            "mts" -> logo.set(R.drawable.mts)
            "tele2" -> logo.set(R.drawable.tele2)
            "beeline" -> logo.set(R.drawable.beeline)
            "megafon" -> logo.set(R.drawable.megafon)
            "yota" -> logo.set(R.drawable.yota)
        }
    }

}