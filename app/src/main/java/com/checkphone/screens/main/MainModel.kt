package com.checkphone.screens.main

import android.graphics.Movie
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.checkphone.network.PhonesApi
import com.checkphone.network.pojo.Phone
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainModel {

    val phoneData: MutableLiveData<Phone> = MutableLiveData<Phone>()

    //==================================================================================================================

    fun requestCheckPhone(phone: String) {
        GlobalScope.launch {
            val response = PhonesApi.Retrofit2.makeRetrofitService().requestCheckPhone(phone).await()

            if (response.isSuccessful) {
                phoneData.postValue(response.body())
                Log.d("ml", "MainModel.requestCheckPhone")
            }
        }
    }

}